import { Component } from '@angular/core';
import { Empleados } from './empleados';

@Component({
	selector: 'empleados',
	templateUrl: './empleados.component.html',
	styleUrls: ['./empleados.component.css']
})

export class EmpleadosComponent{
	public titulo = 'Componente Empleados'
	
	public empleados:Empleados;
	public trabajadores:Array<Empleados>;

	constructor(){
		this.empleados = new Empleados('David Lopez', 45, 'Cocinero', true);
		this.trabajadores = [
			new Empleados('Manolo Martinez', 33, 'Administrador', true),
			new Empleados('Ana Pereyra', 25, 'Cocinero', true),
			new Empleados('Vcitor Robles', 66, 'Programador', false)
		];
	}


	ngOnInit(){
		
		console.log(this.empleados);
		console.log(this.trabajadores);
	}
}