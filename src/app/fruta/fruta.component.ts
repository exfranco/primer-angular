import { Component } from '@angular/core';

@Component({
	selector: 'fruta',
	templateUrl: './fruta.component.html'
	
})
export class FrutaComponent{
	public nombre_componente = 'Componente de fruta';
	public listado_frutas = 'Naranja, Manzana, Pera y Sandia';

	/*public nombre:string = 'David';
	public edad:number = 66;
	public mayorDeEdad:boolean = true;
	public trabajos:Array<string> = ['Carpintero', 'Electricista', 'Fontanero'];
	public extra:Array<any> = ['Carpintero', 44, 'Fontanero'];
	comodin:any = 'Cualquier cosa';*/

	public nombre:string;
	public edad:number;
	public mayorDeEdad:boolean;
	public trabajos:Array<string> = ['Carpintero', 'Electricista', 'Fontanero'];
	public extra:Array<any> = ['Carpintero', 44, 'Fontanero'];
	comodin:any;

	constructor(){
		/*var trabajos = 'fas';
		console.log(trabajos);*/	

		this.nombre ='Franco Saavedra';
		this.edad = 77;
		this.mayorDeEdad = false;
		this.comodin = "SI";

		console.log(this.trabajos);
		console.log(this.comodin);

		/*this.holaMundo(this.nombre);*/
	}

	ngOnInit(){
		this.cambiarNombre();
		this.cambiarEdad(45);
		//alert(this.nombre + " " + this.edad);
		console.log(this.nombre + " " + this.edad);


		// Variables y alcance
		var uno = 8;
		var dos = 15;

		if(uno === 8){
			let uno = 3;
			var dos = 88;
			console.log("Dentro del IF "+uno+ " " +dos);
		}

		console.log("Fuera del IF "+uno+ " " +dos);
	}

	/*holaMundo(nombre){
		alert('Hola mundo!!' + nombre);
	}*/

	cambiarNombre(){
		this.nombre = 'Fischer Tirado';		
	}

	cambiarEdad(edad){
		this.edad = edad;		
	}
}